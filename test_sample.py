import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Chrome
from selenium.webdriver.common.action_chains import ActionChains


class TestSample(object):

    def test_create_todo(self):
        todo = TodoPage()
        todo.add_todo('text of todo')
        assert True, todo.find_todo('text of todo')
        todo.driver.close()

    def test_edit_todo(self):
        todo = TodoPage()
        todo.add_todo('text of todo')
        todo.edit_todo('text of todo', 'new todo text')
        assert True, todo.find_todo('new todo text')
        todo.driver.close()

    def test_delete_todo(self):
        todo = TodoPage()
        todo.add_todo('text of todo')
        todo.delete_todo('text of todo')
        assert todo.find_todo('text of todo')
        todo.driver.close()



class TodoPage(object):

    def __init__(self):
        self.driver = Chrome('/usr/bin/chromedriver')
        self.driver.implicitly_wait(5)
        self.driver.get('http://todomvc.com/examples/react/#/')

        # Создаем тут локаторы, чтобы они находились в одном месте, для удобства
        self.locators = {
            'header': {
                'add_todo': '//*[@data-reactid=".0.0.1"]'
            },
            'main': {
                'toggle_all': '//*[@data-reactid=".0.1.1"]',
                'todo_list': '//*[@data-reactid=".0.1.2"]',
                'todo_item': '//*[contains(text(), "%s")]/..',
                'mark_completed': '//input[@data-reactid="%s"]',
                'delete_from_list': '//button[@data-reactid="%s"]',
                'row_for_update': './*[@data-reactid="%s"]',
                'update_input': '//input[@data-reactid="%s"]',
                'delete_button': '//button[@data-reactid="%s"]'
            },
            'footer': {

            }
        }


    # метод нужен, если мы при объявлении локатора точно не можем знать, какой именно он будет
    # метод на лету будет объединять локатор с атрибутами, которые в процессе выполнения ты узнаешь
    def _resolve_dict_item(self, dict_item, text_to_resolve):
        item = dict_item % text_to_resolve
        return item

    def _find_parent_element(self, element):
        return element.find_element_by_xpath('./..')

    # это костыль, из-за того, что не всегда можно обратиться напрямую к локатору и необходимо
    # модифицировать его в процессе для каких-либо манипуляций
    # сам костыль появился из-за того, что сайт постоянно генерирует новые локаторы и они не статичны из-за
    # гуида внутри локатора
    def _prepare_to_edit(self, string, changing):
        return string + changing

    def add_todo(self, todo_text):
        element = self.driver.find_element_by_xpath(self.locators.get('header').get('add_todo'))
        element.send_keys(todo_text)
        element.send_keys(u'\ue006')

    def edit_todo(self, old_todo_text, new_todo_text):
        action_chain = ActionChains(self.driver)

        # ищем уже созданную todo
        element = self.find_todo(old_todo_text)

        # в процессе нам понадобится атрибут reactid, поэтому получаем его
        react_id = element.get_attribute('data-reactid')
        element = self._find_parent_element(element)

        # модифицируем локатор, чтобы найти конкретный дочерний элемент
        locator = self.locators.get('main').get('row_for_update')
        locator = self._resolve_dict_item(locator, react_id)
        element = element.find_element_by_xpath(locator)

        # двойной клик
        action_chain.double_click(element).perform()

        # изменяем локатор для поиска соседнего локаторая в DOM модели
        # а именно удаляем последний символ и добавляем символ, указанный вторым параметром
        react_id = self._prepare_to_edit(react_id[:-1], '1')
        locator_input = self.locators.get('main').get('update_input')
        locator_input = self._resolve_dict_item(locator_input, react_id)

        element = self.driver.find_element_by_xpath(locator_input)
        element.clear()
        element.send_keys(new_todo_text)
        element.send_keys(u'\ue006')

    def find_todo(self, todo_text):
        try:
            locator = self.locators.get('main').get('todo_item')
            xpath = self._resolve_dict_item(locator, todo_text)
            return self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return True

    def delete_todo(self, todo_text):
        action = ActionChains(self.driver)
        element = self.find_todo(todo_text)
        action.move_to_element(element).perform()

        react_id = element.get_attribute('data-reactid')
        react_id = self._prepare_to_edit(react_id, '.2')
        locator = self.locators.get('main').get('delete_button')
        locator = self._resolve_dict_item(locator, react_id)
        button_delete = self.driver.find_element_by_xpath(locator)
        button_delete.click()

